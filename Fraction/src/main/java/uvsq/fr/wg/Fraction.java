package src.main.java.fr.uvsq.bogoss;


public class Fraction {
	private int numerateur;
	private int denominateur;
	
	//Constantes
	static Fraction ZERO = new Fraction(0, 1);
	static Fraction UN = new Fraction(1, 1);
	
	/**
	 * Constructeur
	 * @param numerateur
	 * @param denominateur
	 */
	Fraction (int numerateur, int denominateur) {
		this.numerateur = numerateur;
		this.denominateur = denominateur;
	}
	
	/**
	 * Constructeur
	 * @param numerateur
	 */
	Fraction (int numerateur) {
		this.numerateur = numerateur;
		this.denominateur = 1;
	}
	/**
	 * Constructeur
	 */
	Fraction () {
		this.numerateur = 0;
		this.denominateur = 1;
	}
	
	public int getNum() {
		return this.numerateur;
	}
	
	public int getDen() {
		return this.denominateur;
	}
	
	public float getVal() {
		return (this.numerateur/this.denominateur);
	}
	
	public String getString() {
		return this.numerateur + "/" + this.denominateur;
	}
}
